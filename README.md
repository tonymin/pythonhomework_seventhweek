# python课程第七周作业

#### 项目介绍
问题描述
本周课程主要讲解Python网络爬虫的基础内容。具体有：Python中的正则表达式，网络爬虫基础，以及urllib、urllib3和requests库的使用。本周闯关作业具体如下（可下载资料包查看更详尽的内容）：
1）.分别使用userlib和requests爬取有道翻译的信息，要求输如英文后获取对应的中文翻译信息：
2 ). 分页爬取58同城的租房信息，信息内容要求有：【标题、图片、户型、价格】，并且获取指定页的所有租房信息:如URL地址：http://bj.58.com/dashanzi/chuzu/pn1/?ClickID=1
3）. 爬取猫眼电影中榜单栏目中TOP100榜的所有电影信息（10页信息全部爬取），字段要求【序号、图片、电影名称、主演、时间、评分】，并将信息写入文件中: 具体参考URL地址：http://maoyan.com/board/4
解题提示
1、有道翻译信息的爬取可参考本周百度翻译信息爬取案例。
2、58同城的租房信息获取，首先先确定URL地址，编写爬虫程序，要使用正则表达式解析爬取的信息。
3、爬取猫眼电影中榜单栏目中TOP100榜的所有电影信息

#### 软件架构
软件架构说明


#### 安装教程

1. xxxx
2. xxxx
3. xxxx

#### 使用说明

1. xxxx
2. xxxx
3. xxxx

#### 参与贡献

1. Fork 本项目
2. 新建 Feat_xxx 分支
3. 提交代码
4. 新建 Pull Request


#### 码云特技

1. 使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2. 码云官方博客 [blog.gitee.com](https://blog.gitee.com)
3. 你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解码云上的优秀开源项目
4. [GVP](https://gitee.com/gvp) 全称是码云最有价值开源项目，是码云综合评定出的优秀开源项目
5. 码云官方提供的使用手册 [http://git.mydoc.io/](http://git.mydoc.io/)
6. 码云封面人物是一档用来展示码云会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)